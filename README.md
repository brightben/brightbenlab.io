# brightfaucher.gitlab.io

My personal portfolio site, hosted via [GitLab Pages](https://about.gitlab.com/features/pages/) for simplicity.

I use the [Minimal Mistakes](https://mmistakes.github.io/minimal-mistakes/) theme for its extensibility.
