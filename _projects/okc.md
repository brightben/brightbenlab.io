---
title: "OKComputer Language"
excerpt: "A fictional programming language specification."
---

OKComputer is a fictional programming language I came up with for a project in my advanced writing class at Northeastern. The concept behind it was a language for complete coding novices. Designing the language was an exercise in genre analysis, thus I only wrote a manual and never implemented it, but the project was also an opportunity to practice software documentation in a way I had never before.

I posted the manual on the public web via GitHub Pages. More info [faucherb94.github.io/ok_computer_site](https://faucherb94.github.io/ok_computer_site).